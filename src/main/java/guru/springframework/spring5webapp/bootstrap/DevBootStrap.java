package guru.springframework.spring5webapp.bootstrap;

import guru.springframework.spring5webapp.model.Author;
import guru.springframework.spring5webapp.model.Book;
import guru.springframework.spring5webapp.model.Publisher;
import guru.springframework.spring5webapp.repositories.AuthorRepository;
import guru.springframework.spring5webapp.repositories.BookRepository;
import guru.springframework.spring5webapp.repositories.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootStrap implements ApplicationListener<ContextRefreshedEvent> {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    @Autowired
    public DevBootStrap(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private  void initData() {

        Author dejton = new Author("Patryk", "Dejton");
        Book ddd = new Book("Domain Driven Design", "1234");
        Publisher p1 = new Publisher("Helion", "Warszawa");
        dejton.getBooks().add(ddd);
        ddd.getAuthors().add(dejton);
        ddd.setPublisher(p1);

        authorRepository.save(dejton);
        publisherRepository.save(p1);
        bookRepository.save(ddd);

        Author owca = new Author("Karol", "Owca");
        Book noEJB = new Book("J2EE without EJB", "256987");
        Publisher p2 = new Publisher("Collins", "London");
        owca.getBooks().add(noEJB);
        noEJB.setPublisher(p2);

        authorRepository.save(owca);
        publisherRepository.save(p2);
        bookRepository.save(noEJB);
    }
}
